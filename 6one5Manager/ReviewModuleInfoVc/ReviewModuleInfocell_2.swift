//
//  ReviewModuleInfocell_2.swift
//  6one5Manager
//
//  Created by enyotalearning on 22/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewModuleInfocell_2: UITableViewCell {
    @IBOutlet weak var descriptorLabel2: UILabel!
    @IBOutlet weak var moduleDescription2: UILabel!
    @IBOutlet weak var descriptorTitleLabel2: UILabel!
    @IBOutlet weak var cell2NextButton: UIButton!
    @IBOutlet weak var cell2previousButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
