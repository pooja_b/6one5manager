//
//  WebAPI.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class WebAPI
{
    static let SALESREPORTFEEDBACK: String = "http://key2train.in/admin/api/v1/feedback/"
    static let SALESREPORTQUIZSUBMITFORREVIEW: String = "http://key2train.in/admin/api/v1/module_data/"
    static let LOGIN: String = "http://key2train.in/admin/api/v1/login"
    static let SALESREPORTMODULEINFORMATION: String = "http://key2train.in/admin/api/v1/fetch_learner_manager_review/"
    static let MANAGERGETNOTIFS: String = "http://key2train.in/admin/api/v1/module_review_request/"
    static let MANAGERSUBMITRATING: String = "http://key2train.in/admin/api/v1/manager_descriptor_ratings/"
    static let MANAGERLEARNERLIST:String = "http://key2train.in/admin/api/v1/learners/"
    static let INDIVIDUALPERFORMANCE:String = "http://key2train.in/admin/api/v1/performance/"
    static let MANAGERCERTIFICATE:String = "http://key2train.in/admin/api/v1/send_certificate/"
    static let TEAMPERFORMANCE:String = "http://key2train.in/admin/api/v1/team_performance/"
    static let MODULEREVIEWREQUEST:String = "https://key2train.in/admin/api/v1/module_review_request/"
    
    static let USERNOTIFICATION:String = "https://key2train.in/admin/api/v1/user_notifications/"
    static let UPDATEUSERNOTIFICATION:String = "https://key2train.in/admin/api/v1/update_user_notification/"
    static let GETBADGESLIST:String = "https://key2train.in/admin/api/v1/get_badges_list/"
    static let GETORIENTATIONBADGESLIST:String = "https://key2train.in/admin/api/v1/get_orientation_badges_list/"
    static let GETACHIVEMENTBADGESLIST:String = "https://key2train.in/admin/api/v1/get_achievement_badges_list/"
    
    //Static Change after final API
    static let SENDCERTIFICATE:String = "https://key2train.in/admin/api/v1/send_certificate/41/2"
    static let USERBADGESNOTIFICATION:String = "https://key2train.in/admin/api/v1/user_badges_notification/5"
    static var navigationforBadges = ""
    /*
     
     static let SALESREPORTFEEDBACK: String = "https://key2train.in/admin/api/v1/feedback/"
     static let SALESREPORTQUIZSUBMITFORREVIEW: String = "https://key2train.in/admin/api/v1/module_data/"
     static let LOGIN: String = "https://key2train.in/admin/api/v1/login"
     static let SALESREPORTMODULEINFORMATION: String = "https://key2train.in/admin/api/v1/fetch_learner_manager_review/"
     static let MANAGERGETNOTIFS: String = "https://key2train.in/admin/api/v1/module_review_request/"
     static let MANAGERSUBMITRATING: String = "https://key2train.in/admin/api/v1/manager_descriptor_ratings/"
     static let MANAGERLEARNERLIST:String = "https://key2train.in/admin/api/v1/learners/"
     static let INDIVIDUALPERFORMANCE:String = "https://key2train.in/admin/api/v1/performance/"
     
     static let TEAMPERFORMANCE:String = "https://key2train.in/admin/api/v1/team_performance/"
     */

    
    
}
