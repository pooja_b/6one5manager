//
//  Badges.swift
//  6one5Manager
//
//  Created by enyotalearning on 23/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class Badges: NSObject
{
    
    var badges = [BadgesDetails]()
    var message:String!
    var status:String!
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: Badges
    {
        //2
        struct Singleton
        {
            //3
            static let instance = Badges()
        }
        //4
        return Singleton.instance
    }
    
    func addCertificateData(badgesDetils:BadgesDetails)
    {
        badges.append(badgesDetils)
    }
    
    func freeCertificateData()
    {
        badges = [BadgesDetails]()
    }
    
}
