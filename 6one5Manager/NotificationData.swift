//
//  NotificationData.swift
//  6one5Manager
//
//  Created by enyotalearning on 24/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation


class NotificationData
{
    
    var notificationDetailsArray = [NotificationDetails]()
    var currentUerId:String!
    var message:String!
    var status:String!
    
    class var sharedInstance: NotificationData
    {
        //2
        struct Singleton {
            //3
            static let instance = NotificationData()
        }
        //4
        return Singleton.instance
    }
    
    func addNotificationDetails(notificationDetails:NotificationDetails)
    {
        notificationDetailsArray.append(notificationDetails)
    }
    
    func getreiviewDetailsCount() -> Int{
        return notificationDetailsArray.count
    }
    
    func getUserName(index:Int) ->String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.name
    }
    
    /*func getModuleName(index:Int) ->String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.moduleName
    }*/
    func getId(index:Int) -> String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.id
    }
    
    
    func getNotificationDetails(index:Int) -> NotificationDetails
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv
    }
    
    func getRepresentativeId(index:Int) -> String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.representative_id
    }
    
    func setCureentUserId(id:String)
    {
        currentUerId = id
    }
    
    func getCureentReviewData(index:Int) -> NotificationDetails
    {
        return notificationDetailsArray[index];
    }
    
    func getContent(index:Int) -> String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.content
    }
    
    func getNotificationType(index:Int) -> String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.notification_type
    }
    
    func getNotificationId(index:Int) -> String
    {
        let rv:NotificationDetails = notificationDetailsArray[index];
        
        return rv.notification_id
    }
    
    /*func desc()
     {
     let count = reiviewDetailsArray.count - 1
     for index in 0...
     {
     let st = getModuleName(index: index)
     print (" *** user name \(st)")
     }
     }*/

    
    
    
}


class NotificationDetails
{
    var id:String!
    var name:String!
    var content:String!
    var notification_id:String!
    var user_id:String!
    var badge_id:String!
    var course_id:String!
    var module_id:String!
    var representative_id:String!
    var notification_type:String!
    
    
    
    
}
