//
//  ManagerFeedbackService.swift
//  6one5Manager
//
//  Created by enyotalearning on 04/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
/*public class ManagerFeedbackService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: ManagerFeedbackService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = ManagerFeedbackService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(postData:String completionHandler:@escaping (ManagerFeedbackData)->())
    {
        let login = Login.sharedInstance
        
        let apiString = WebAPI.MANAGERSUBMITRATING + login.getManagerId()//Constants.managerSubmitRatingURL + manager_id
        var request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let alertController = UIAlertController(title: "Review Submission", message: "Error in connection. Please try later." , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    self.navigationController?.isNavigationBarHidden = false
                    self.view.removeFromSuperview()
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alertController = UIAlertController(title: "Review Submission", message: "Review submitted successfully" , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "Review Submission", message: "Review submission failed due to unresponsive server. Please try later." , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
                
                
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
    
}*/
