//
//  ChartConfig.swift
//  6one5Manager
//
//  Created by enyotalearning on 15/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import Charts

class ChartConfig
{
    
    
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: ChartConfig
    {
        //2
        struct Singleton {
            //3
            static let instance = ChartConfig()
        }
        //4
        return Singleton.instance
    }
    
    func setBarChartData(dataPoints: [String], values: [Double], chartObject:BarChartView!, chartLabel:String)
    {
        if values.count > 1 && validateChartData(values: values)
        {
            var yValues : [BarChartDataEntry] = [BarChartDataEntry]()
            
            for i in 0 ..< dataPoints.count {
                yValues.append(BarChartDataEntry(x: Double(i + 1), y: values[i]))
                
            }
            
            let data = BarChartData()
            let ds = BarChartDataSet(values: yValues, label: chartLabel)
            
            data.addDataSet(ds)
            chartObject.data = data
            
            let colorConfig =  ColorConfig.sharedInstance
            let barColor = colorConfig.getBarColors()
            
            var colors: [UIColor] = []
            
            for index in 0..<dataPoints.count {
                
                if index > barColor.count - 1 // If there is new element more than 6 it will use dynamic color
                {
                    
                    let red = Double(arc4random_uniform(256))
                    let green = Double(arc4random_uniform(256))
                    let blue = Double(arc4random_uniform(256))
                    
                    let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
                    colors.append(color)
                    
                }
                else
                {
                    let color = barColor[index]
                    colors.append(color)
                }
            }
            
            ds.colors = colors
            
            //chartObject.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBack)
            let l = chartObject.legend
            
            l.setCustom(colors: colors, labels: dataPoints)
        }
        else
        {
            chartObject.noDataText = "No Data for creating chart"
        }
    }

    
    func setPieChartData(dataPoints: [String], values: [Double], chartObject:PieChartView!, chartLabel:String)
    {
        if values.count > 1 && validateChartData(values: values)
        {
            print("I am here in the pie chart count is \(values.count)")
            var yValues : [ChartDataEntry] = [ChartDataEntry]()
            
            
            for i in 0 ..< dataPoints.count {
                yValues.append(ChartDataEntry(x: Double(i + 1), y: values[i]))
                
            }
            
            let data = PieChartData()
            let ds = PieChartDataSet(values: yValues, label: chartLabel)
            //data.setValue(dataPoints, forKey: "Mo")
            
            
            data.addDataSet(ds)
            chartObject.data = data
            
            
            let colorConfig =  ColorConfig.sharedInstance
            let barColor = colorConfig.getBarColors()
            
            var colors: [UIColor] = []
            
            for index in 0..<dataPoints.count {
                
                if index > barColor.count  - 1 // If there is new element more than 6 it will use dynamic color
                {
                    
                    let red = Double(arc4random_uniform(256))
                    let green = Double(arc4random_uniform(256))
                    let blue = Double(arc4random_uniform(256))
                    
                    let color = UIColor(red: CGFloat(red/255), green: CGFloat(green/255), blue: CGFloat(blue/255), alpha: 1)
                    colors.append(color)
                    
                }
                else
                {
                    let color = barColor[index]
                    colors.append(color)
                }
            }
            
            ds.colors = colors
            
            
            //chartObject.animate(xAxisDuration: 2.0, yAxisDuration: 2.0, easingOption: .easeInBounce)
            let l = chartObject.legend
            
            l.setCustom(colors: colors, labels: dataPoints)
        }
        else
        {
            print("I am here in the pie chart no data ");
            chartObject.noDataText = "No Data for creating chart"
        }
    }

    
    //Hack : I need todo something where all data has 0 values. you can see legends only.
    
    func validateChartData(values:[Double]) -> Bool
    {
         for index in 0..<values.count
         {
            if values[index] != 0
            {
                return true
            }
         }
            
        return false
    }
    
    
}
