//
//  HomeVCCell.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class HomeVCCell: UICollectionViewCell
{
    @IBOutlet weak var ManagerHomeCVCellGridImg: UIImageView!
    
    @IBOutlet weak var ManagerHomeCVCellGridLbl: UILabel!
}
