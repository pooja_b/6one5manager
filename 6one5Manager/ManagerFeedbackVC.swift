//
//  ManagerFeedbackVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 30/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class ManagerFeedbackVC: UIViewController,UIPickerViewDataSource,UIPickerViewDelegate  {
    //, DKDropMenuDelegate
    @IBOutlet weak var descriptor1: CosmosView!
    @IBOutlet weak var descriptor2: CosmosView!
    @IBOutlet weak var descriptor3: CosmosView!
    //@IBOutlet weak var dropdownView: DKDropMenu!
    
    @IBOutlet weak var descriptor4: CosmosView!
    @IBOutlet weak var modulePicker: UIPickerView!
    
    
    @IBOutlet weak var AddReviewTitle: UILabel!
    @IBOutlet weak var moduleTitle: UILabel!
    @IBOutlet weak var descText1: UILabel!
    @IBOutlet weak var descText2: UILabel!
    
    @IBOutlet weak var descText3: UILabel!
    @IBOutlet weak var descText4: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var cancelBtn: UIButton!
    
    var module_id = 2
    var course_id = 2
    var user_id: String = "4"
    var manager_id: String = "165"
    
    var salesrep_id = "1"
    var salesmodule_id = "1"
    var modulePickerArray = [String]()
    var moduleIdArray = [String]()
    var courseIdArray = [String]()
    
    var modelReviewRequest = ModelReviewRequest.sharedInstance
    
    var descripters = [["1discripter1".localized(),"1discripter2".localized(), "1discripter3".localized() ],
                       ["2discripter1".localized(),"2discripter2".localized(), "2discripter3".localized()],
                       ["3discripter1".localized(),"3discripter2".localized(), "3discripter3".localized(), "Cross & up sells"],
                       
                       ["4discripter1".localized(),"4discripter2".localized(), "4discripter3".localized()],
                       ["5discripter1".localized(),"5discripter2".localized(), "5discripter3".localized()],
                       ["6discripter1".localized(),"6discripter2".localized(), "6discripter3".localized()]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Here is user id \(user_id)")
        modulePicker.dataSource = self
        modulePicker.delegate = self
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "plusbtnImage"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(playTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    func playTapped (){
        
    }
    override func viewWillAppear(_ animated: Bool)
    {
        addDataToView()
    }
    func addDataToView()
    {
        let login = Login.sharedInstance
        manager_id = login.getManagerId()
        let learnerDetails =  LearnerDetails.sharedInstance
        user_id = learnerDetails.selectedLearnerId//learnerDetails.getLearnerId(index: learnerDetails.selectedLearner)
        
        print( " user id is \(user_id)")
        
        //module_id = reviewNotificationData
        if learnerDetails.navigationPath == "2"
        {
            //Only selected Module review
            let notificationData = NotificationData.sharedInstance
            let notificationObject = notificationData.getNotificationDetails(index: learnerDetails.notificationIndex)
            modulePickerArray = [notificationObject.name]
            moduleIdArray = [notificationObject.module_id]
            courseIdArray = [notificationObject.course_id]
            module_id = Int(notificationObject.module_id)!
            setDescripterText(index:module_id)
        }
        else
        {
            //loadModelReviewRequestData(representativeId: user_id)
            getModelReviewNotificationNotifs()
            print("Here is the Module review req count \(modelReviewRequest.modelReviewRequestArray.count)")
        }
    }
    
    func setDescripterText(index:Int)
    {
        descText1.text = descripters[index - 1][0]
        descText2.text = descripters[index - 1][1]
        switch module_id {
        case 3:
            descText3.text = descripters[index - 1][2]
            descriptor3.alpha = 1
            descText3.alpha = 1
            descText4.text = descripters[index - 1][3]
            descText4.alpha = 1
            descriptor4.alpha = 1
            break
        case 5:
            
            descriptor3.alpha = 0
            descText3.alpha = 0
            descText4.alpha = 0
            descriptor4.alpha = 0
            break
        default:
            
            descText3.text = descripters[index - 1][2]
            descriptor3.alpha = 1
            descText3.alpha = 1
            descText4.alpha = 0
            descriptor4.alpha = 0
            break
            
        }
        
        //        if module_id == 5
        //        {
        //            descriptor3.alpha = 0
        //            descText3.alpha = 0
        //            descText4.alpha = 0
        //            descriptor4.alpha = 0
        //        }
        //        else
        //        {
        //            descText3.text = descripters[index - 1][2]
        //            descriptor3.alpha = 1
        //            descText3.alpha = 1
        //            descText4.alpha = 0
        //            descriptor4.alpha = 0
        //        }
    }
    
    func loadModelReviewRequestData(representativeId:String)
    {
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if (modelReviewRequest.getRequestCount() >= 0 )
        {
            //print("reviewNotificationData Data Reset ")
            //write clear logic here
            modelReviewRequest.modelReviewRequestArray = [ModelReviewRequestData]()
            //learnerDetails.learnerProfileArray = [String]()
        }
        
        ModelReviewRequestService.sharedInstance.loadData(reprenentativeId: representativeId, completionHandler: { (userData:ModelReviewRequest) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (self.modelReviewRequest.getRequestCount() >= 0 )
            {
                
                let login = Login.sharedInstance
                progressHUD.hide()
                
                UIApplication.shared.endIgnoringInteractionEvents()
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
                    
                    
                else
                {
                    
                    DispatchQueue.main.async()
                        {
                            print("Model Review request main execute")
                            progressHUD.hide()
                            //self.reviewNotificationData.desc()
                            
                            self.addDataToView()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                    }
                }
                
                
                //learner details service completed
                
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
        
    }
    
    func itemSelected(withIndex: Int, name: String) {
        print("\(name) selected --- index \(withIndex+1)");
        module_id = withIndex+1
    }
    
    @IBAction func submitCancelClicked(_ sender: AnyObject) {
        self.navigationController?.isNavigationBarHidden = false
        self.view.removeFromSuperview()
    }
    
    @IBAction func reviewSubmitClicked(_ sender: AnyObject) {
        
        //print("Des1: \(self.descriptor1.rating)")
        //print("Des2: \(self.descriptor2.rating)")
        //print("Des3: \(self.descriptor3.rating)")
        submitReview()
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return modulePickerArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return modulePickerArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //print("Here you have selected language is : \(modulePickerArray[row]) " );
        
        //language.setLanguage(language: languages[row])
        module_id = Int(moduleIdArray[row])!
        course_id = Int(courseIdArray[row])!
        
        
        self.setDescripterText(index:self.module_id)
        
        print(" Selected Module Name: \(modulePickerArray[row]) module id is :\(module_id)  Course id : \(course_id) ")
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = modulePickerArray[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 26.0)!,NSForegroundColorAttributeName:UIColor.black])
        return myTitle
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1;
    }
    
    
    func submitReview()
    {
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        let desc1 = self.descriptor1.rating
        let desc2 = self.descriptor2.rating
        let desc3 = self.descriptor3.rating
        let desc4 = self.descriptor4.rating
        //course_id = 2
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let dataStr: String!
        
        switch module_id {
        case 5:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String("") as NSString)\" } }]"
            break
        case 3:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String(desc3) as NSString)\",\"4\":\"\(String(desc4) as NSString)\"} }]"
            break
        default:
            dataStr = "data=[{ \"user_id\":\"\(user_id as NSString)\",\"module_id\":\"\(String(module_id) as NSString)\",\"course_id\":\"\(String(course_id) as NSString)\",\"descriptor\": { \"1\":\"\(String(desc1) as NSString)\",\"2\":\"\(String(desc2) as NSString)\",\"3\":\"\(String(desc3) as NSString)\" } }]"
        }
        
        //        if module_id != 5 && module_id !== 3
        //        {
        //
        //
        //        }else if module_id == 3{
        //
        //
        //        }
        //        else
        //        {
        //
        //        }
        
        //print("Data string is \(dataStr)")
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        let apiString = WebAPI.MANAGERSUBMITRATING + manager_id//Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "connectionerror".localized() , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                    self.navigationController?.isNavigationBarHidden = false
                    self.view.removeFromSuperview()
                    self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                progressHUD.hide()
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    //print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "reviewsubmitsucess".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    let alertController = UIAlertController(title: "reviewsubmission".localized(), message: "unresponsiveserver".localized() , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        self.navigationController?.isNavigationBarHidden = false
                        self.view.removeFromSuperview()
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    progressHUD.hide()
                    
                }
                
                
                
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
    
    
    func getModelReviewNotificationNotifs()
    {
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let apiURL: String = "https://key2train.in/admin/api/v1/module_review_request/" + user_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (error != nil) {
                print("Network Error: \(error)")
                let alert = UIAlertController(title: "modelreviewsubmission".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary 111 ",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("status value is \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                
                                let modelReviewRequest = ModelReviewRequest.sharedInstance
                                let json = JSON(convertedJsonIntoDict)
                                var modelReviewRequestData:ModelReviewRequestData
                                for item in json["data"].arrayValue
                                {
                                    modelReviewRequestData = ModelReviewRequestData()
                                    
                                    modelReviewRequestData.id = item["id"].stringValue
                                    modelReviewRequestData.name = item["name"].stringValue
                                    modelReviewRequestData.l_fname = item["l_fname"].stringValue
                                    modelReviewRequestData.l_lname = item["l_lname"].stringValue
                                    modelReviewRequestData.module_id = item["module_id"].stringValue
                                    modelReviewRequestData.course_id = item["course_id"].stringValue
                                    
                                    self.modulePickerArray.append(item["name"].stringValue)
                                    self.moduleIdArray.append(item["module_id"].stringValue)
                                    self.courseIdArray.append(item["course_id"].stringValue)
                                    
                                    modelReviewRequest.addModelReviewRequest(modelReviewRequestData: modelReviewRequestData)
                                }
                                
                                print("Here is module id 88 \(self.moduleIdArray[0])")
                                self.module_id = Int(self.moduleIdArray[0])!
                                self.setDescripterText(index:self.module_id)
                                self.modulePicker.reloadAllComponents()
                                
                                progressHUD.hide()
                                
                                
                                
                            }
                        } else
                        {
                            /*var error_msg:NSString
                             if convertedJsonIntoDict["message"] as? NSString != nil
                             {
                             error_msg = convertedJsonIntoDict["message"] as! NSString
                             } else {
                             error_msg = "Unknown Error"
                             }
                             print("error_msg",error_msg)*/
                            
                            progressHUD.hide();
                            
                            
                            let alertController = UIAlertController(title: "modelreviewsubmission", message: "No review request" , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.popViewController(animated: true)
                                self.navigationController?.isNavigationBarHidden = false
                                self.view.removeFromSuperview()
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    progressHUD.hide()
                    
                    
                }
                
                
            }
        })
        
        dataTask.resume()
    }
    
    
    
}
