//
//  SalesTeamPerformanceVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Charts
import Localize_Swift

class SalesTeamPerformanceVC: UIViewController {
    
    
    @IBOutlet weak var containerView: UIView!
    var pageContainerHeight:Int!
    var moduleVC:ModuleVC!
    var levelVC:LevelsVC!
    //var certificateVC:CertificateVC!
    var badgesVC:BadgesVC!
    var currentView:String = "ModuleVC"
    
    
    @IBOutlet weak var modules: UILabel!
    @IBOutlet weak var levels: UILabel!
    @IBOutlet weak var badges: UILabel!
    
    
    @IBOutlet weak var moduleIcon: UIButton!
    @IBOutlet weak var levelsIcon: UIButton!
    @IBOutlet weak var badgesIcon: UIButton!
    
    let salesTeamPerformanceData =  SalesTeamPerformanceData.sharedInstance
    let badgesChartOrientationData = BadgesChartOrientaionData.sharedInstance
    let badgesChartAchievementData = BadgesChartAchievementData.sharedInstance
    
    override func viewWillAppear(_ animated: Bool)
    {

        modules.text = "sales_modules".localized()
        levels.text = "sales_levels".localized()
        badges.text = "sales_badges".localized()
        
        //self.title = "sales_team_performance".localized()
        
        loadData()
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    }
    
    func loadData()
    {
        // loading modules data : This hack we have to download the chart data before screen load so, :This is hack :)
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        if (self.salesTeamPerformanceData.getQuizDataCount() > 0 )
        {
            //write clear logic here
            salesTeamPerformanceData.quizDataPoints = [String]()
            salesTeamPerformanceData.reviewDataPoints = [String]()
            
            salesTeamPerformanceData.levelPerformanceDataPoints = [String]()
            salesTeamPerformanceData.courseStatusDataPoint = [String]()
            
            salesTeamPerformanceData.quizDataValues = [Double]()
            salesTeamPerformanceData.reviewDataValues = [Double]()
            
            salesTeamPerformanceData.levelPerformanceDataValues = [Double]()
            salesTeamPerformanceData.courseStatusDataValues = [Double]()
        }
        
        SalesTeamPerformanceService.sharedInstance.loadData(completionHandler: { (userData:SalesTeamPerformanceData) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            let login = Login.sharedInstance
            progressHUD.hide()
            
            UIApplication.shared.endIgnoringInteractionEvents()
            if login.status != nil
            {
                let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                    (action:UIAlertAction!) in
                    login.logoutFromDeviceCache()
                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
            }
                
                
            else
            {
            if (self.salesTeamPerformanceData.getQuizDataCount() > 0 )
            {
                //loading badges chart data
                          DispatchQueue.main.async()
                    {
                        self.loadBadgesChartOrientationData()
                        //self.loadBadgesChartAchievementData()
                        progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        
                } //learner details service completed*/
               
            }// end if learnerDetails.getLearnerCount
            else
            {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            }
        })
        
    }
    
    func loadBadgesChartOrientationData()
    {
        if (self.badgesChartOrientationData.badgesOrientationArray.count >= 0 )
        {
            badgesChartOrientationData.freeBadgesOrientationData()
        }
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        print("Starting the certificate service")
        BadgesChartOrientationService.sharedInstance.loadData(completionHandler:
            { (badgesChartOrientaionData:BadgesChartOrientaionData) -> () in
    
                DispatchQueue.main.async()
                    {
                        
                        //print("Stop the certificate service")
                        progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.loadBadgesChartAchievementData()
                }
        })
        
    }
    
    func loadBadgesChartAchievementData()
    {
        if (self.badgesChartAchievementData.badgesAchievmentArray.count >= 0 )
        {
            badgesChartAchievementData.freeBadgesOrientationData()
        }
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        print("Starting the certificate service")
        BadgesChartAchivementService.sharedInstance.loadData(completionHandler:
            { (badgesChartAchievementData:BadgesChartAchievementData) -> () in

                DispatchQueue.main.async()
                    {
                        print("Stop the certificate service")
                        progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        self.setUpPage()
                }
        })
        print("Badges Chart Data completed")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpPage()
    {
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
        
        moduleVC = ModuleVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
        
        
        print("Page Button setup Module vc height is \(size) and screen size is \(screenHeight)")
        //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
        containerView.addSubview(moduleVC)
        //mySubView.sizeToFit()
        currentView = "ModuleVC"
        
        removeCurrentVC()
        // This hack to prevent the update
        print("Module Button setup Module vc height is \(size) and screen size is \(screenHeight)")
        //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
        moduleVC = ModuleVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
        
        
        //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
        containerView.addSubview(moduleVC)
        currentView = "ModuleVC"
        moduleIcon.alpha = 0.7
        moduleIcon.isEnabled = false
        
        levelsIcon.alpha = 1
        levelsIcon.isEnabled = true
        
        
        badgesIcon.alpha = 1
        badgesIcon.isEnabled = true
        
        
    }
    @IBAction func modulesButtonTapped(_ sender: UIButton)
    {
        removeCurrentVC()
        
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        print("Module Button setup Module vc height is \(size) and screen size is \(screenHeight)")
        //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
        moduleVC = ModuleVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
        
        
        //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
        containerView.addSubview(moduleVC)
        currentView = "ModuleVC"
        moduleIcon.alpha = 0.7
        moduleIcon.isEnabled = false
    }
    
    @IBAction func levelsButtonTapped(_ sender: UIButton)
    {
        removeCurrentVC()
        
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
        levelVC = LevelsVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
        
        
        //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
        containerView.addSubview(levelVC)
        currentView = "LevelsVC"
        levelsIcon.alpha = 0.7
        levelsIcon.isEnabled = false
    }
    
    @IBAction func badgesButtonTapped(_ sender: UIButton)
    {
        removeCurrentVC()
        
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
        badgesVC = BadgesVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
        
        
        //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
        containerView.addSubview(badgesVC)
        currentView = "BadgesVC"
        badgesIcon.alpha = 0.7
        badgesIcon.isEnabled = false
    }
    
    /*@IBAction func certificateButtonTapped(_ sender: UIButton)
     {
     removeCurrentVC()
     
     let screenSize: CGRect = UIScreen.main.bounds;
     let screenWidth = screenSize.width;
     let screenHeight = screenSize.height;
     //pageContainerHeight = Int(screenHeight) - (Int(header.frame.height) + Int(footer.frame.height) + Int(header.frame.origin.y))
     certificateVC = CertificateVC(frame: CGRect(x: 0, y: 0, width:screenWidth, height: screenHeight))
     
     
     //print("@@@@@@@@@@@@@ containerView.frame.width " + String(screenWidth) + " Height :" + String(screenHeight) + "Page Height " + String(pageContainerHeight) )
     containerView.addSubview(certificateVC)
     currentView = "CertificateVC"
     }*/
    
    func removeCurrentVC()
    {
        if currentView == "ModuleVC"
        {
            moduleVC.view.removeFromSuperview();
            moduleIcon.alpha = 1
            moduleIcon.isEnabled = true
        }
        if currentView == "LevelsVC"
        {
            levelVC.view.removeFromSuperview();
            levelsIcon.alpha = 1
            levelsIcon.isEnabled = true
        }
        /* if currentView == "CertificateVC"
         {
         certificateVC.view.removeFromSuperview();
         }*/
        if currentView == "BadgesVC"
        {
            badgesVC.view.removeFromSuperview();
            badgesIcon.alpha = 1
            badgesIcon.isEnabled = true
        }
    }
}
