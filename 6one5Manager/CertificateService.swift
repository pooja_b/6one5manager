//
//  CertificateService.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
import UIKit


class CertificateService
{
    
    class var sharedInstance: CertificateService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = CertificateService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (Certificate)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        
        //let login = Login.sharedInstance
        
        let apiURL: String = WebAPI.SENDCERTIFICATE
        //WebAPI.MANAGERCERTIFICATE + "4/2" // login.getManagerId() + "/2" // "https://api.enyotalms.com/lms/test/admin/api/v1/learners/41"
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil)
            {
                print("Error: \(error)")
                let certificates = Certificate.sharedInstance
                completionHandler(certificates)
            } //end of error if
            else
            {
                //let httpResponse = response as? HTTPURLResponse
                print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                let certificates = Certificate.sharedInstance
                                //var path = convertedJsonIntoDict["data"] as? String
                                //certificates.path = path
                                
                                let json = JSON(convertedJsonIntoDict)
                                var certDetails:CertificateDetails
                                for item in json["data"].arrayValue
                                {
                                    certDetails = CertificateDetails()
                                    certDetails.id = item["id"].stringValue
                                    certDetails.path = item["path"].stringValue
                                    if item == 1 // Avoid mobile data
                                    {
                                        let url = URL(string: certDetails.path)
                                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                        certDetails.image = UIImage(data: data!)
                                    }
                                    certificates.addCertificateData(certificateDetails: certDetails)
                                    print(" Maigc ids " + item["id"].stringValue)
                                }
                                
                                completionHandler(certificates)
                            }//end if
                        }
                        
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }// End else
            
            
        })
        
        dataTask.resume()
    }
    
}


