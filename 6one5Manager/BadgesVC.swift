//
//  BadgesVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Localize_Swift
import Charts

@IBDesignable class BadgesVC: UIView {

    var view:UIView!
    
    @IBOutlet weak var badgesTitle: UILabel!
    @IBOutlet weak var orientaionLabel: UILabel!
    @IBOutlet weak var achievementLabel: UILabel!
   
    @IBOutlet weak var chart1: BarChartView!
    @IBOutlet weak var chart2: BarChartView!
    
    
    @IBOutlet weak var achievementsLabel: UILabel!
    @IBOutlet weak var orientationLabel: UILabel!
     let badgesChartOrientaionData =  BadgesChartOrientaionData.sharedInstance
     let badgesChartAchievementData =  BadgesChartAchievementData.sharedInstance
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "Bagdes" , bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        //badgesTitle.text = "badgesTitle".localized()
        
        return view
    }
    
    func setup() {
        view = loadViewFromNib()
        
        let screenSize: CGRect = UIScreen.main.bounds;
        let screenWidth = screenSize.width;
        let screenHeight = screenSize.height;
        
        //view.frame = bounds
        //view.frame = self.frame
        //view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] //UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing. FlexibleHeightaddSubview(view)
        view.frame = CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight - 200 )//CGRect(0, 0, self.frame.size.width, self.frame.size.height);
        load()
        addSubview(view)
        
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
            scrollView.contentOffset.y = 0
        }
    }
    
    func load()
    {
        //let dollars1 = [10.0, 20.0, 30.0]
        //let months = ["Module 1", "Module 2", "Module 3"]
        
        print ("Here is the count of the badges id \(badgesChartOrientaionData.countValues.count)")
        let chart = ChartConfig.sharedInstance
        
        //quiz data
        chart.setBarChartData(dataPoints: badgesChartOrientaionData.idValues, values: badgesChartOrientaionData.countValues, chartObject: chart1, chartLabel: "Chart 1")
        chart1.chartDescription?.text = ""
        //chart1.legend.position = .rightOfChartCenter
        
        //chart1.noDataText = "No Data for creating chart"
        
        
        //BarChart
        chart.setBarChartData(dataPoints: badgesChartAchievementData.idValues, values: badgesChartAchievementData.countValues, chartObject: chart2, chartLabel: "Chart 2")
        //chart2.legend.position = .rightOfChartCenter
        chart2.chartDescription?.text = ""
        //chart2.noDataText = "No Data for creating chart"
        chart1.dragDecelerationEnabled = false
        //chart1.rotationEnabled = false
        
        chart2.dragDecelerationEnabled = false
        // chart2.rotationEnabled = false
    }


}
