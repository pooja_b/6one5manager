//
//  ManagerBadgesVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ManagerBadgesVC: UIViewController {
    let learnerDetails =  LearnerDetails.sharedInstance
    
    @IBOutlet weak var badgeCheckMark1: UIButton!
    @IBOutlet weak var badgeCheckMark2: UIButton!
    @IBOutlet weak var badgeCheckMark3: UIButton!
    @IBOutlet weak var badgeCheckMark4: UIButton!
    @IBOutlet weak var badgeCheckMark5: UIButton!
    
    @IBOutlet weak var badgeView1: UIView!
    
    @IBOutlet weak var badgeView2: UIView!
    
    @IBOutlet weak var badgeView3: UIView!
    
    @IBOutlet weak var badgeView4: UIView!
    
    @IBOutlet weak var badgeView5: UIView!
    
    
    @IBOutlet weak var checkmarkImage1: UIImageView!
    @IBOutlet weak var checkmarkImage2: UIImageView!
    
    @IBOutlet weak var checkmarkImage3: UIImageView!
    
    @IBOutlet weak var checkmarkImage4: UIImageView!
    
    @IBOutlet weak var checkmarkImage5: UIImageView!
    
    @IBOutlet weak var badgeHolder1: UILabel!
    @IBOutlet weak var badgeHolder2: UILabel!
    @IBOutlet weak var badgeHolder3: UILabel!
    @IBOutlet weak var badgeHolder4: UILabel!
    @IBOutlet weak var badgeHolder5: UILabel!
    
    
    @IBOutlet weak var badgeButton1: UIButton!
    @IBOutlet weak var badgeButton2: UIButton!
    @IBOutlet weak var badgeButton3: UIButton!
    @IBOutlet weak var badgeButton4: UIButton!
    @IBOutlet weak var badgeButton5: UIButton!
    
    
    var badges = BadgesData.sharedInstance
    var assignedTo = "Assigned to "
    var currentBadgeId:String!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Badges"
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataBadgesDetails()
        let btn1 = UIButton(type: .custom)
        btn1.setImage(UIImage(named: "plusbtnImage"), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(infoTapped), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
    }
    func infoTapped (){
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//     let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
//        UIView.beginAnimations("animation", context: nil)
//        UIView.setAnimationDuration(1.0)
//        self.navigationController!.pushViewController(viewController, animated: false)
//        UIView.setAnimationTransition(UIViewAnimationTransition.flipFromLeft, for: self.navigationController!.view, cache: false)
//        UIView.commitAnimations()
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
       // let viewController:ReviewInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewInfoViewController") as! ReviewInfoViewController
        let viewController:ReviewModuleInfoViewController = storyboard.instantiateViewController(withIdentifier: "ReviewModuleInfoViewController") as! ReviewModuleInfoViewController
        self.navigationController?.present(viewController, animated: true, completion: nil)
    }
    
    func loadDataBadgesDetails()
    {
        if (self.badges.repBadgesDetailsArray.count >= 0 )
        {
            //TODO Free the badges from array.
            badges.freeBadgesData()
        }
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        print(" @@@ Starting the badges service")
        BadgesForRepresentative.sharedInstance.loadData(completionHandler: { (userData:BadgesData) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            let login = Login.sharedInstance
            progressHUD.hide()
            UIApplication.shared.endIgnoringInteractionEvents()
            if login.status != nil
            {
                let alertController = UIAlertController(title: "User Status", message: login.message , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                    (action:UIAlertAction!) in
                    login.logoutFromDeviceCache()
                    self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
            }
                
                
            else
            {
                DispatchQueue.main.async() {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.assignBadgesUI()
                }
            }
        })
    }
    
    
    func assignBadgesUI()
    {
        
        for index in 0 ... badges.repBadgesDetailsArray.count - 1
        {
            print("Index running \(index)")
            let badge = badges.getBadgesDetails(index: index)
            
            var name:String = ""
            
            if badge.fristName != nil
            {
                name = badge.fristName
            }
            if badge.lastName != nil
            {
                name += " " + badge.lastName
            }
            print(" Here are name assigned \(name)")
            assignBadgeName(badgeId: badge.id, badgeHolderName: name)
            
            
        }
    }
    
    
    func assignBadgeName(badgeId:String, badgeHolderName:String)
    {
        if(badgeHolderName != "")
        {
            switch (badgeId)
            {
            case "9":
                badgeHolder1.text = assignedTo + badgeHolderName
                badgeView1.backgroundColor = UIColor.green
                checkmarkImage1.image = UIImage( named :"tickBtnImage")
                badgeButton1.isEnabled = false
            case "10":
                badgeHolder2.text = assignedTo + badgeHolderName
                badgeView2.backgroundColor = UIColor.green
                checkmarkImage2.image = UIImage( named :"tickBtnImage")
                badgeButton2.isEnabled = false
            case "11":
                badgeHolder3.text = assignedTo + badgeHolderName
                badgeView3.backgroundColor = UIColor.green
                checkmarkImage3.image = UIImage( named :"tickBtnImage")
                badgeButton3.isEnabled = false
            case "12":
                badgeHolder4.text = assignedTo + badgeHolderName
                badgeView4.backgroundColor = UIColor.green
                checkmarkImage4.image = UIImage( named :"tickBtnImage")
                badgeButton4.isEnabled = false
            case "13":
                badgeHolder5.text = assignedTo + badgeHolderName
                badgeView5.backgroundColor = UIColor.green
                checkmarkImage5.image = UIImage( named :"tickBtnImage")
                badgeButton5.isEnabled = false
            default:
                print("--- Unspecified Badge")
            }
        }
        
    }
    
    @IBAction func callAssignBadgeScrren(_ sender: AnyObject) {
        print("call assign Badge \(sender.tag)")
        //representativelistfromiphone
        currentBadgeId = String(8 + sender.tag)
        
        if WebAPI.navigationforBadges == "homevc" {
            performSegue(withIdentifier: "representativelistfromiphone", sender: sender)
            
        }else{
            
            self.submitBadgeToRepresentative(learnerId: learnerDetails.selectedLearnerId)
            loadDataBadgesDetails()
        }
        
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "representativelistfromiphone")
        {
            
            let repList = (segue.destination as! RepresentativeListVC)
            repList.badgeId = currentBadgeId
        }
        
    }
    func submitBadgeToRepresentative(learnerId:String)
    {
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        
        //let badgesId = "3"
        let userId = learnerId
        let courseId = "2"
        
        let headers = [
            "cache-control": "no-cache"
        ]
        
        let dataStr: String!
        
        dataStr = "data=[{ \"badge_id\":\"\(currentBadgeId as NSString)\",\"user_id\":\"\(String(userId) as NSString)\",\"course_id\":\"\(String(courseId) as NSString)\" }]"
        
        
        
        print("Data string is here \(dataStr)")
        
        let postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
        //print("Post string is \(dataStr.data(using: String.Encoding.utf8))")
        /*let dataStr: String = "data={ \"uname\" : \"\(self.UserNameTxtField.text! as NSString)\" , \"upass\" : \"\(self.PasswordTxtField.text! as NSString)\" }"
         print("POST Params: \(dataStr)")
         var postData = NSMutableData(data: dataStr.data(using: String.Encoding.utf8)!)
         
         */
        
        let apiString = "https://key2train.in/admin/api/v1/assign_user_badges"//WebAPI.MANAGERSUBMITRATING + manager_id//Constants.managerSubmitRatingURL + manager_id
        let request = NSMutableURLRequest(url: NSURL(string: apiString)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            //print("Magic Data is  \(data!)")
            if (error != nil) {
                print("Error: \(error)")
                let alertController = UIAlertController(title: "badgesubmission".localized(), message: "connectionerror".localized() , preferredStyle: .alert)
                let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                    // self.navigationController?.popViewController(animated: true)
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
                
                progressHUD.hide()
                
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("Response: \(httpResponse)")
                do {
                    
                    //print("Magic Data is  \(data!)")
                    
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print("Status: \(statusVal!)")
                        
                        if(statusVal! == true)
                        {
                            let alertController = UIAlertController(title: "badgesubmission".localized(), message: "badgesubmissionsucess".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                //self.navigationController?.isNavigationBarHidden = false
                                //self.view.removeFromSuperview()
                                // self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            progressHUD.hide()
                            
                            
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                            let alertController = UIAlertController(title: "badgesubmission".localized(), message: "budgesubmissionfailed".localized() , preferredStyle: .alert)
                            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                                _ = self.navigationController?.popViewController(animated: true)
                                //self.navigationController?.popViewController(animated: true)
                            }
                            alertController.addAction(OKAction)
                            self.present(alertController, animated: true, completion:nil)
                            
                            progressHUD.hide()
                            
                        }
                    }
                } catch let error as NSError {
                    print(error)
                    let alertController = UIAlertController(title: "badgesubmission".localized(), message: "budgesubmissionfailed".localized() , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in
                        //self.navigationController?.popViewController(animated: true)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                    progressHUD.hide()
                    
                }
                print("Data: \(data)")
            }
        })
        
        dataTask.resume()
    }
 }
