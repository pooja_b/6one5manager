//
//  ReviewModuleInfoViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 22/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewModuleInfoViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var reviewModuleinfoTableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    @IBAction func closeButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      //  var cell :UITableViewCell!
        if indexPath.row == 0 {
              // let cell = reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1", for: indexPath)

           let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_1") as! ReviewModuleInfocell_1
            cell.cell1NextButton.tag = indexPath.row
            cell.cell1NextButton.addTarget(self, action: #selector(cell1NextButtonAction),for: .touchUpInside)
           // cell.cell1NextButton.addTarget(self, action: Selector("cell1NextButtonAction:"), for: UIControlEvents.touchUpInside);
            cell.cell1PreviousButton.tag = indexPath.row
            cell.cell1PreviousButton.addTarget(self, action: Selector(("cell1PreviouButton:")), for: UIControlEvents.touchUpInside);
            return cell
        }//        case 1:
//            cell = reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_2", for: indexPath)
//            cell.cell2NextButton.tag = indexPath.row
//            cell.cell2NextButton.addTarget(self, action: "cell2NextButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            cell.cell2previousButton.tag = indexPath.row
//            cell.cell2previousButton.addTarget(self, action: "cell2previousButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            break
//        case 2:
//            cell = reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_3", for: indexPath)
//            cell.cell3NextButton.tag = indexPath.row
//            cell.cell3NextButton.addTarget(self, action: "cell3NextButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            cell.cell3PreviousButton.tag = indexPath.row
//            cell.cell3PreviousButton.addTarget(self, action: "cell3PreviousButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            break
//        case 3:
//            cell = reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4", for: indexPath)
//            cell.cell4NextButton.tag = indexPath.row
//            cell.cell4NextButton.addTarget(self, action: "cell4NextButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            cell.cell4PreviousButton.tag = indexPath.row
//            cell.cell4PreviousButton.addTarget(self, action: "cell4PreviousButton:", forControlEvents: UIControlEvents.TouchUpInside);
//            break
        else{
            let cell = self.reviewModuleinfoTableview.dequeueReusableCell(withIdentifier: "ReviewModuleInfocell_4") as! ReviewModuleInfocell_4
            cell.cell4NextButton.tag = indexPath.row
            cell.cell4NextButton.addTarget(self, action: Selector(("cell4NextButton:")), for: UIControlEvents.touchUpInside);
            cell.cell4PreviousButton.tag = indexPath.row
            cell.cell4PreviousButton.addTarget(self, action: Selector(("cell4PreviousButton:")), for: UIControlEvents.touchUpInside);
            return cell
        }
//        cell.selectionStyle =  UITableViewCellSelectionStyle.none
    }
    func cell1NextButtonAction(sender:UIButton!){
    
    }
    func cell2NextButton(sender:UIButton){
        
    }
    func cell3NextButton(sender:UIButton){
        
    }
    func cell4NextButton(sender:UIButton){
        
    }
 
    
    func cell1PreviouButton(sender:UIButton){
        
    }
    func cell2previousButton(sender:UIButton){
        
    }
    func cell3PreviousButton(sender:UIButton){
        
    }
    func cell4PreviousButton(sender:UIButton){
        
    }

   
    
}
