//
//  HelpTableViewCell.swift
//  6one5Manager
//
//  Created by enyotalearning on 19/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {
    @IBOutlet weak var helpTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
