//
//  SalesTeamPerformanceData.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class SalesTeamPerformanceData: NSObject
{
    var quizDataPoints = [String]()
    var reviewDataPoints = [String]()
    
    var levelPerformanceDataPoints = [String]()
    var courseStatusDataPoint = [String]()
    
    var quizDataValues = [Double]()
    var reviewDataValues = [Double]()
   
    var levelPerformanceDataValues = [Double]()
    var courseStatusDataValues = [Double]()
    
    var message:String!
    var status:String!
    
    class var sharedInstance: SalesTeamPerformanceData
    {
        //2
        struct Singleton {
            //3
            static let instance = SalesTeamPerformanceData()
        }
        //4
        return Singleton.instance
    }
    
    func addQuizDataPoints(dataPoint:String)
    {
        quizDataPoints.append(dataPoint)
    }
    
    func addQuizDataValues(dataValue:Double)
    {
        quizDataValues.append(dataValue)
    }
    
    func addReviewDataPoints(dataPoint:String)
    {
        reviewDataPoints.append(dataPoint)
    }
    
    func addReviewDataValues(dataValue:Double)
    {
        reviewDataValues.append(dataValue)
    }
    
    func addLevelPerformanceDataPoints(dataPoint:String)
    {
        levelPerformanceDataPoints.append(dataPoint)
    }
    
    func addLevelPerformanceDataValues(dataValue:Double)
    {
        levelPerformanceDataValues.append(dataValue)
    }
    
    func addCourseStatusDataPoints(dataPoint:String)
    {
        courseStatusDataPoint.append(dataPoint)
    }
    
    func addCourseStatusDataValues(dataValue:Double)
    {
        courseStatusDataValues.append(dataValue)
    }
    
    func getQuizDataCount() -> Int
    {
        return quizDataPoints.count
    }
    
}
