//
//  HelpViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 19/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {
    @IBOutlet weak var helpWebview: UIWebView!
    @IBOutlet weak var helpTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let localfilePath = Bundle.main.url(forResource: "English", withExtension: "html");
        let myRequest = NSURLRequest(url: localfilePath!);
        self.helpWebview.loadRequest(myRequest as URLRequest);
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
