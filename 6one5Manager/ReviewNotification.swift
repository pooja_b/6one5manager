//
//  ReviewNotification.swift
//  6one5Manager
//
//  Created by enyotalearning on 03/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Localize_Swift

class ReviewNotification: UIViewController , UITableViewDelegate,UITableViewDataSource
{
    
    @IBOutlet weak var tableView: UITableView!
    var selectedIndex:Int!
    var notificationData = NotificationData.sharedInstance
    let learnerDetails =  LearnerDetails.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        let login = Login.sharedInstance
        login.notificationcount = 0
        self.tabBarController?.tabBar.items![3].badgeValue = nil
        //self.title = "review_notifications".localized()
        loadLernerData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadLernerData()
    {
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        if (self.learnerDetails.getLearnerCount() >= 0 )
        {
            print("reviewNotification learner Data Reset ")
            
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerId = [String]()
            learnerDetails.selectedLearner = -1
            learnerDetails.performanceData = [IndividualPerformanceData]()
            
        }
        
        LearnersDetailsService.sharedInstance.loadData(completionHandler: { (userData:LearnerDetails) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (self.learnerDetails.getLearnerCount() >= 0 )
            {
                DispatchQueue.main.async(execute:
                    {
                        print("Here is the Learner data loaded")
                        //self.imageView.image = image;
                }) //learner details service completed
                
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("error While loading learner data")
            } // end else learnerDetails.getLearnerCount
            
        })
        
        
        if (self.notificationData.getreiviewDetailsCount() >= 0 )
        {
            //print("reviewNotificationData Data Reset ")
            //write clear logic here
            notificationData.notificationDetailsArray = [NotificationDetails]()
            //learnerDetails.learnerProfileArray = [String]()
        }
        
        NotificationService.sharedInstance.loadData(completionHandler: { (userData:NotificationData) -> () in
            DispatchQueue.main.async(){
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            }
            if (self.notificationData.getreiviewDetailsCount() >= 0 )
            {
                let login = Login.sharedInstance
                progressHUD.hide()
                
                UIApplication.shared.endIgnoringInteractionEvents()
                
                if login.status != nil
                {
                    
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }
                    
                    
                else
                {
                    
                    DispatchQueue.main.async()
                        {
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.tableView.reloadData()
                            
                    } //learner details service completed
                }
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
        
        
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        //print(self.items.count)
        return notificationData.getreiviewDetailsCount() ;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell") as! ReviewNotificationCell
        
        cell.name.text = notificationData.getContent(index: indexPath.row)
        //+ " has requested for coaching  and review session of " + reviewNotificationData.getModuleName(index: indexPath.row)
        
        //cell.textLabel?.text = reviewNotificationData.getUserName(index: indexPath.row) + " has requested for coaching  and review session of " + reviewNotificationData.getModuleName(index: indexPath.row)
        //self.items[indexPath.row]
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle
    {
        if notificationData.getNotificationType(index: indexPath.row) == "3"
        {
            return UITableViewCellEditingStyle.none
        }
        else
        {
            return UITableViewCellEditingStyle.delete
        }
        /*let object = items[indexPath.row]
         if object.name == "joyce" {
         return UITableViewCellEditingStyle.Delete
         } else {
         return UITableViewCellEditingStyle.None
         }*/
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath)
    {
        if editingStyle == .delete
        {
            print("Delete request found \(indexPath.row) ")
            
            
            let notificationId = notificationData.getId(index: indexPath.row)
            removeNotification(notificationId: notificationId)
            
            tableView.reloadData()
            //let status = removeNotification()
            //TODO: Here Manager asked to delete this notification
            // This is normal notification
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        //let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        
        selectedIndex = (indexPath?.row)!
        
        print("tableView cell selected now : \(selectedIndex)")
        
        
        let learnerDetails =  LearnerDetails.sharedInstance
        
        let lId:String = notificationData.getRepresentativeId(index: indexPath!.row)
        notificationData.setCureentUserId(id: lId)
        //print("Review lear id:  \(lId)")
        //learnerDetails.printLearnerid()
        //let index:Int = learnerDetails.getIndexforLearnerid(lId: lId)
        //print("Lerner index : \(index)")
        //learnerDetails.setSelectedLearner(index: index)
        
        learnerDetails.setSelectedLearnerId(id: lId)
        learnerDetails.navigationPath = "2"
        learnerDetails.notificationIndex = indexPath!.row
        
        if notificationData.getNotificationType(index: indexPath!.row) == "3"
        {
            let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagerAddReviewPopupScroll") as! ManagerFeedbackVC
            
            self.addChildViewController(popOverVC)
            popOverVC.view.frame = self.view.frame
            
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParentViewController: self)
            
            self.navigationController?.isNavigationBarHidden = true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        
        
        print("Review Prepare func init")
        
        let indexPath  = self.tableView.indexPathForSelectedRow!
        
        let learnerDetails =  LearnerDetails.sharedInstance
        
        let lId:String = notificationData.getRepresentativeId(index: indexPath.row)
        notificationData.setCureentUserId(id: lId)
        //print("Review lear id:  \(lId)")
        //learnerDetails.printLearnerid()
        //let index:Int = learnerDetails.getIndexforLearnerid(lId: lId)
        //print("Lerner index : \(index)")
        //learnerDetails.setSelectedLearner(index: index)
        
        learnerDetails.setSelectedLearnerId(id: lId)
        
    }
    
    
    func removeNotification(notificationId:String)
    {
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        RemoveUserNotification.sharedInstance.loadData(notificationId:notificationId, completionHandler: { (status:Bool) -> () in
            DispatchQueue.main.async() {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
            }
            if (self.notificationData.getreiviewDetailsCount() >= 0 )
            {
                let login = Login.sharedInstance
                progressHUD.hide()
                
                UIApplication.shared.endIgnoringInteractionEvents()
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                    
                }
                else
                {
                    
                    DispatchQueue.main.async()
                        {
                            self.loadLernerData()
                            progressHUD.hide()
                            
                          
                            //self.reviewNotificationData.desc()
                            self.tableView.reloadData()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            
                    } //learner details service completed
                }
            }// end if learnerDetails.getLearnerCount
            else
            {
                print("Problem while loading data ")
                let alert = UIAlertController(title: "notificationerror".localized(), message: "errordetails".localized(), preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                
            } // end else learnerDetails.getLearnerCount
            
        })
    }
    
}
