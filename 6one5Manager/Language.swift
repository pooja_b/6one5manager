//
//  Language.swift
//  6one5Manager
//
//  Created by enyotalearning on 07/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

public class Language: NSObject
{
    //assets_home_btn assets_performance_btn_Page 1
    //assets_home_btn_selected assets_performance_btn_selected
   
     var currentLanguage = "en"
    //Singleton pattern
    //This is to make sure will use only one object for creating the login
    class var sharedInstance: Language
    {
        //2
        struct Singleton {
            //3
            static let instance = Language()
        }
        //4
        return Singleton.instance
    }
    
    //This is init method but I have not used the default method just because of the future purpose and we can achive the singleton pattern
    
    func setLanguage(language:String)
    {
        currentLanguage = language;
    }
    
    func getLanguage() -> String
    {
        return currentLanguage;
    }
}
