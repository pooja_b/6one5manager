//
//  IndividualPerformanceVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Charts
import Localize_Swift

class IndividualPerformanceVC: UIViewController {

    
    @IBOutlet weak var chart1: BarChartView!
    @IBOutlet weak var chart2: BarChartView!
    @IBOutlet weak var chart3: PieChartView!
    @IBOutlet weak var headerView: UIView!
    
    //Data modules for the chart
    var quizData:NSDictionary!
    var reviewData:NSDictionary!
    var moduleStatus:NSDictionary!
    var progressHUD:ProgressHUD!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    //@IBOutlet weak var addReview: UIButton!
    //@IBOutlet weak var addNotes: UIButton!
    
    @IBOutlet weak var quizTitle: UILabel!
    @IBOutlet weak var reviewTitle: UILabel!
    @IBOutlet weak var moduleStatusTitle: UILabel!
    //let individualPerformanceData =  IndividualPerformanceData.sharedInstance
    let learnerDetails = LearnerDetails.sharedInstance
    var performace:IndividualPerformanceData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let selected =  learnerDetails.getSelectedLearner()
        print("##### learner id \(learnerDetails.learnerId) swl \(selected)")
        performace = learnerDetails.getPerformanceDataAtIndex(index: selected)
        setCharts()
    }
     @IBAction func addNotesTapped(_ sender: UIButton) {
        
    }

    @IBAction func addReviewTapped(_ sender: UIButton)
    {
        
        let popOverVC = UIStoryboard (name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ManagerAddReviewPopupScroll") as! ManagerFeedbackVC
        
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func setCharts()
    {
        let chart = ChartConfig.sharedInstance
        
        chart.setBarChartData(dataPoints: performace.quizDataPoints, values: performace.quizDataValues, chartObject: chart1, chartLabel: "Chart 1")
        chart1.chartDescription?.text = ""
        chart.setBarChartData(dataPoints: performace.reviewDataPoints, values: performace.reviewDataValues, chartObject: chart2, chartLabel: "Chart 2")
        chart2.chartDescription?.text = ""
        chart.setPieChartData(dataPoints: performace.moduleStatusPoints, values: performace.moduleStatusValues, chartObject: chart3, chartLabel: "Chart 3")
        chart3.chartDescription?.text = ""
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool)
    {
        //addNotes.setTitle("addnotes".localized(), for: UIControlState.normal)
        //addReview.setTitle("addreview".localized(), for: .normal)
        //self.title = "individual_performance".localized()
        quizTitle.text = "quiz".localized()
        reviewTitle.text = "review".localized()
        moduleStatusTitle.text = "module_status".localized()
        
    }

    
    override func viewWillLayoutSubviews()
    {
        super.viewWillLayoutSubviews();
        let bounds = UIScreen.main.bounds
        let height = bounds.size.height
        
        self.scrollView.frame = self.view.bounds; // Instead of using auto layout
        let deviceType = DeviceUtil.getDeviceType()
        if deviceType == 2
        {
            self.scrollView.contentSize.height = height * 1.7 // Or whatever you want it to be.
        }
    }
    
    @IBAction func BadgesButtonClick(_ sender: AnyObject) {
        WebAPI.navigationforBadges = ""
        WebAPI.navigationforBadges = "individualperformancevc"
    }
    
}//end of class
