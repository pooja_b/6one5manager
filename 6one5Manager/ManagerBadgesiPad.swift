//
//  ManagerBadgesiPad.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ManagerBadgesiPad: UIViewController {

    @IBOutlet weak var badgeCheckMark1: UIButton!
    @IBOutlet weak var badgeCheckMark2: UIButton!
    @IBOutlet weak var badgeCheckMark3: UIButton!
    @IBOutlet weak var badgeCheckMark4: UIButton!
    @IBOutlet weak var badgeCheckMark5: UIButton!
    
    @IBOutlet weak var badgeHolder1: UILabel!
    @IBOutlet weak var badgeHolder2: UILabel!
    @IBOutlet weak var badgeHolder3: UILabel!
    @IBOutlet weak var badgeHolder4: UILabel!
    @IBOutlet weak var badgeHolder5: UILabel!
    
    
    @IBOutlet weak var badgeButton1: UIButton!
    @IBOutlet weak var badgeButton2: UIButton!
    @IBOutlet weak var badgeButton3: UIButton!
    @IBOutlet weak var badgeButton4: UIButton!
    @IBOutlet weak var badgeButton5: UIButton!
    
    
    var badges = BadgesData.sharedInstance
    var assignedTo = "Assigned to "
    var currentBadgeId:String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadDataBadgesDetails()
    }
    
    func loadDataBadgesDetails()
    {
        if (self.badges.repBadgesDetailsArray.count >= 0 )
        {
            //TODO Free the badges from array.
            badges.freeBadgesData()
        }
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        print("Starting the badges service")
        BadgesForRepresentative.sharedInstance.loadData(completionHandler: { (userData:BadgesData) -> () in
            
            DispatchQueue.main.async() {
            print("Stop the badges service")
            progressHUD.hide()
            UIApplication.shared.endIgnoringInteractionEvents()
            self.assignBadgesUI()
            }
        })
    }
    
    
    func assignBadgesUI()
    {
        
        for index in 0 ... badges.repBadgesDetailsArray.count - 1
        {
            print("Index running \(index)")
            let badge = badges.getBadgesDetails(index: index)
            
            var name:String = ""
            
            if badge.fristName != nil
            {
                name = badge.fristName
            }
            if badge.lastName != nil
            {
                name += " " + badge.lastName
            }
            print(" Here are name assigned \(name)")
            assignBadgeName(badgeId: badge.id, badgeHolderName: name)
            
            
        }
    }
    
    
    func assignBadgeName(badgeId:String, badgeHolderName:String)
    {
        if(badgeHolderName != "")
        {
            switch (badgeId)
            {
            case "9":
                badgeHolder1.text = assignedTo + badgeHolderName
                badgeCheckMark1.alpha = 1
                badgeButton1.isEnabled = false
            case "10":
                badgeHolder2.text = assignedTo + badgeHolderName
                badgeCheckMark2.alpha = 1
                badgeButton2.isEnabled = false
            case "11":
                badgeHolder3.text = assignedTo + badgeHolderName
                badgeCheckMark3.alpha = 1
                badgeButton3.isEnabled = false
            case "12":
                badgeHolder4.text = assignedTo + badgeHolderName
                badgeCheckMark4.alpha = 1
                badgeButton4.isEnabled = false
            case "13":
                badgeHolder5.text = assignedTo + badgeHolderName
                badgeCheckMark5.alpha = 1
                badgeButton5.isEnabled = false
            default:
                print("--- Unspecified Badge")
            }
        }
        
    }
    
    @IBAction func callAssignBadgeScrren(_ sender: AnyObject) {
        
        print("call assign Badge \(sender.tag)")
        currentBadgeId = String(8 + sender.tag)
        performSegue(withIdentifier: "managerbadgesipad", sender: sender)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if(segue.identifier == "managerbadgesipad")
        {
            
            let repList = (segue.destination as! RepresentativeListVC)
            repList.badgeId = currentBadgeId
        }
        
    }
    

}
