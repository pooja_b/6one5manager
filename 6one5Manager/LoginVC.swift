//
//  LoginVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Localize_Swift

class LoginVC: UIViewController,UITextFieldDelegate, UIPickerViewDataSource,UIPickerViewDelegate {
    
    @IBOutlet weak var languagePicker: UIPickerView!
    @IBOutlet weak var UserNameTxtField: UITextField!
    @IBOutlet weak var PasswordTxtField: UITextField!
    @IBOutlet weak var LoginButton: UIButton!
    
    let language = Language.sharedInstance
    let availableLanguages = Localize.availableLanguages()
    
    
    let login =  Login.sharedInstance
    let progressHUD = ProgressHUD()
    
    let languages = ["English","Chinese","Arabic"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.isNavigationBarHidden = true
        UserNameTxtField.delegate = self
        PasswordTxtField.delegate = self
        language.setLanguage(language: "en")
        
        languagePicker.dataSource = self
        languagePicker.delegate = self
        self.hideKeyboardWhenTappedAround()
        let pre = NSLocale.preferredLanguages[0]
        print(" Here is current language \(pre) ");
        
        // Do any additional setup after loading the view.
    }

    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return languages.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return languages[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print("Here you have selected language is : \(languages[row]) " );
        
        language.setLanguage(language: languages[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = languages[row]
        let myTitle = NSAttributedString(string: titleData, attributes: [NSFontAttributeName:UIFont(name: "Georgia", size: 26.0)!,NSForegroundColorAttributeName:UIColor.black])
        return myTitle
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1;
    }
    
    /**
     * Called when 'return' key pressed. return NO to ignore.
     */
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    /**
     * Called when the user click on the view (outside the UITextField).
     */
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
      language.setLanguage(language: languages[0])
      let defaults = UserDefaults.standard

       //out.text = "copyright".localized()

        if let isLogged = defaults.string(forKey: "isLoggedIn")
        {
            let le = defaults.string(forKey: "isLoggedIn")!
        print("User logged from cache \(isLogged)" )
        if(le == "1")
        {
            let login = Login.sharedInstance
         //   print("**************** \(defaults.string( forKey: "praferdLanguage")!)")
          

            login.setup(
                        userName:  defaults.string(forKey: "username")!,
                        password: defaults.string(forKey: "password")!,
                        id: defaults.string( forKey: "id")!,
                        courseId: defaults.string( forKey: "courseId")!,
                        isLoggedIn: "1",
                        fname: defaults.string( forKey: "fName")!,
                        lName: defaults.string( forKey: "lName")!,
                        language: defaults.string( forKey: "praferdLanguage")!,
                        notification_count: defaults.integer(forKey:"notification_count")
                        )
            login.setErrorMessage(msg: "")
            
            //print("Direct home screen")
            DispatchQueue.main.async()
                {
                //self.imageView.image = image;
                
                self.language.setLanguage(language: login.getPraferdLanguage())
                let languageUtility = LanguageUtility.sharedInstance
                languageUtility.updateLanguage()
                
                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "homeviewController") as UIViewController
                self.present(initViewController, animated: true, completion: nil)
                
                self.progressHUD.hide()
            }
        }
        }

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func LoginButtonTapped(_ sender: UIButton)
    {
        UserNameTxtField.resignFirstResponder();
        PasswordTxtField.resignFirstResponder();
        
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        var username = self.UserNameTxtField.text
        var password = self.PasswordTxtField.text
        
        
        if ( username=="" || password=="" )
        {
            let alertController = UIAlertController(title: "signinfailed".localized() , message: "signinerro".localized() , preferredStyle: .alert)
            let OKAction = UIAlertAction(title: "ok".localized(), style: .default) { (action:UIAlertAction!) in }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
            progressHUD.hide()

        }
        
        else
        {
            if (username=="p" || password=="p")
            {
                username = "jamessmith2@6one5.com"
                password = "Admin@123"
            }
//            username = UserNameTxtField.text
//            password = PasswordTxtField.text
            
            if (1 == 1 )//login.getIsLogged() == "true" // user cache
            {
                LoginService.sharedInstance.validateLoginDetailsOnline(userName: username!, passwd: password!, preferdLanguage: language.getLanguage() ,completionHandler: { (userData:Login) -> () in
                
                    DispatchQueue.main.async() {
                        self.progressHUD.hide()
                        UIApplication.shared.endIgnoringInteractionEvents()
                    }

                    let islog:String = self.login.getIsLogged()
                    //print("Is log in found \(islog)" )
                    if (islog == "1" )
                    {
                         let languageUtility = LanguageUtility.sharedInstance
                         languageUtility.updateLanguage()
                        self.login.saveLoginToDevice()
                        DispatchQueue.main.async()
                            {
                                                              //self.imageView.image = image;
                                let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let initViewController: UIViewController = storyboard.instantiateViewController(withIdentifier: "homeviewController") as UIViewController
                                self.present(initViewController, animated: true, completion: nil)
                                self.progressHUD.hide()
                        }
                    }
                    else
                    {
                         DispatchQueue.main.async() {
                        let alert = UIAlertController(title: "loginerror".localized(), message: self.login.errorMessage, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok".localized(), style: UIAlertActionStyle.default, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        self.progressHUD.hide()
                            
                         }
                    }
                    
                    
                    
                    
                })
                
            }
        }
        
        
    } //end of load data
}//end of the class

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
