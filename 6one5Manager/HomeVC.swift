//
//  HomeVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Foundation
import Localize_Swift

class HomeVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    var login =  Login.sharedInstance

    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet var DashboardbackUIView: UIView!
    
    @IBOutlet weak var managerHomeCollectionV: UICollectionView!
    
    @IBOutlet weak var profileName: UILabel!
    var listOfImages: [UIImage] = [ UIImage(named: "certificate.png")!,
                                    UIImage(named: "module.png")!,
                                    UIImage(named: "review.png")!,
                                    UIImage(named: "badges.png")!,
                                    UIImage(named: "faqs.png")!,
                                    UIImage(named: "tnc.png")!
    ]
    
    var listOfDashboardItem = ["certificate".localized() ,"modules".localized() , "performance".localized(),"badges".localized(),"faq".localized(),"termsandcondition".localized()]
    
    
    //HOMEVC
    fileprivate let reuseIdentifier = "ManagerHomeGridCell"
    //    private let sectionInsets = UIEdgeInsets(top: 50.0, left: 20.0, bottom: 50.0, right: 20.0)
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! HomeVCCell
        // Configure the cell
        cell.ManagerHomeCVCellGridImg.image = listOfImages[(indexPath as NSIndexPath).row]
        cell.ManagerHomeCVCellGridLbl.text = listOfDashboardItem[(indexPath as NSIndexPath).row]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        print((indexPath as NSIndexPath).row)
        switch (indexPath as NSIndexPath).row {
        case 0:
            //let next = self.storyboard?.instantiateViewController(withIdentifier: "CertificateVC") as! ManagerCertificateVC
            //self.present(next, animated: true, completion: nil)
            
            performSegue(withIdentifier: "managerc", sender: nil)
            //performSegue(withIdentifier: "CertificateVC", sender: nil)
            break
            
            
        case 1:
            //            let next = self.storyboard?.instantiateViewControllerWithIdentifier("HomeViewControllerID") as! HomeViewController
            //            self.presentViewController(next, animated: true, completion: nil)
            //            performSegue(withIdentifier: "ManagerDashboardPerfSeg", sender: nil)
            
            
            break
        case 2:
            
            //            let next = self.storyboard?.instantiateViewController(withIdentifier: "ManagerPerformanceView") as! ManagerTabBarTeamViewController
            //            self.present(next, animated: true, completion: nil)
            //            performSegue(withIdentifier: "ManagerPerformanceSegue", sender: nil)
            //            let nextViewController = self.storyboard?.instantiateViewController(withIdentifier: "ManagerPerformanceView")
            //            self.present(nextViewController!, animated:true, completion:nil)
            //            let tabBarCtrl: UITabBarController = (self.navigationController?.tabBarController)! as UITabBarController
            //            tabBarCtrl.selectedIndex = 1
            //            self.navigationController?.tabBarController?.selectedIndex = 1
            
            //performSegue(withIdentifier: "ManagerDashboardPerfSeg", sender: nil)
            tabBarController?.selectedIndex = 1
            break
            
        case 3:
            //            let next = self.storyboard?.instantiateViewControllerWithIdentifier("BadgesCollectionViewControllerID") as! BadgesCollectionViewController
            //            self.presentViewController(next, animated: true, completion: nil)
            //            performSegue(withIdentifier: "BadgesVwCtrlSegueID", sender: nil)
            WebAPI.navigationforBadges = ""
            WebAPI.navigationforBadges = "homevc"
            performSegue(withIdentifier: "ManagerBadgesVC", sender: nil)
            
            break
        default:
            print((indexPath as NSIndexPath).row)
            
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        let widthSize = collectionView.frame.size.width / 2
        print("Here1")
        return CGSize(width: widthSize-1, height: widthSize)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        print("Here2")
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout
        collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        print("Here3")
        return 1.0
    }
    
   // var notifCount = 0
    var manager_id = "165"
    
    override func viewDidLoad() {
        
        let url = "https://key2train.in/admin/api/v1/learners/41"
        
        //doRequestGet(url: url)
            print("Home Screen view Load")
        
        manager_id = login.getManagerId()
        
        
        self.navigationController!.navigationBar.topItem!.title = "";
        super.viewDidLoad()
        profileName.text = login.fName + " " + login.lName
        /*let prefs:UserDefaults = UserDefaults.standard
         manager_id = prefs.value(forKey: "user_id") as! String!*/
        //getNotifs()
        
        let DashBlueColor = UIColor(red: 0/255.0, green: 174/255.0, blue: 239/255.0, alpha: 1.0)
        self.DashboardbackUIView.backgroundColor = DashBlueColor
        
        self.managerHomeCollectionV.delegate = self
        self.managerHomeCollectionV.dataSource = self
        self.managerHomeCollectionV!.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 1)
        
        //let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        
        
        //print("Here!!!")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        print()
        self.navigationController?.isNavigationBarHidden = true
        print(" will apper")
        
        let languageUtility = LanguageUtility.sharedInstance
        languageUtility.updateLanguage()
        
        tabBarController?.tabBar.items![0].title = "home_tab".localized()
        tabBarController?.tabBar.items![1].title = "performance_tab".localized()
        tabBarController?.tabBar.items![2].title = "teams_tab".localized()
        tabBarController?.tabBar.items![3].title = "flag_tab".localized()
        tabBarController?.tabBar.items![4].title = "help".localized()
        
        let noitf_count = self.login.notificationcount
        if noitf_count != 0 {
            tabBarController?.tabBar.items![3].badgeValue = "\(noitf_count)"
        }
        
        logoutButton.setTitle("logout".localized(), for: UIControlState.normal)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool)
    {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    @IBAction func logoutTapped(_ sender: UIButton)
    {
        let login = Login.sharedInstance
        login.logoutFromDeviceCache()
        print("Direct home screen")
        DispatchQueue.main.async(execute: {
            //self.imageView.image = image;
            
            let language = Language.sharedInstance
            language.setLanguage(language: login.getPraferdLanguage())
            let languageUtility = LanguageUtility.sharedInstance
            languageUtility.updateLanguage()
            self.dismiss(animated: true, completion: nil)
          /*  let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let initViewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController as! LoginVC
            self.present(initViewController, animated: true, completion: nil)
            //self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)*/
            
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    /*func getNotifs() {
        let headers = [
            "content-type": "application/x-www-form-urlencoded",
            "cache-control": "no-cache",
            ]
        
        let apiURL: String = WebAPI.MANAGERGETNOTIFS + manager_id // Constants.managerGetNotifsURL + manager_id
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let alert = UIAlertController(title: "Notifications", message: "Error in connection. Please check your internet connection and try again.", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            } else {
                let httpResponse = response as? HTTPURLResponse
                print("HTTP Response: \(httpResponse)")
                
                do {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        print(statusVal!)
                        
                        if(statusVal! == true)
                        {
                            OperationQueue.main.addOperation{
                                
                                if let notifs = convertedJsonIntoDict["data"] as? [[String: AnyObject]] {
                                    self.notifCount = notifs.count
                                }
                            }
                        } else {
                            var error_msg:NSString
                            if convertedJsonIntoDict["message"] as? NSString != nil {
                                error_msg = convertedJsonIntoDict["message"] as! NSString
                            } else {
                                error_msg = "Unknown Error"
                            }
                            print("error_msg",error_msg)
                        }
                    }
                } catch let error as NSError {
                    print(error)
                }
                
                
            }
        })
        
        dataTask.resume()
    }*/
}
