//
//  ModelReviewRequest.swift
//  6one5Manager
//
//  Created by enyotalearning on 25/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class ModelReviewRequest
{
    var modelReviewRequestArray = [ModelReviewRequestData]()
    //var currentUerId:String!
    var message:String!
    var status:String!
    
    
    class var sharedInstance: ModelReviewRequest
    {
        //2
        struct Singleton {
            //3
            static let instance = ModelReviewRequest()
        }
        //4
        return Singleton.instance
    }
    
    func addModelReviewRequest(modelReviewRequestData:ModelReviewRequestData)
    {
        modelReviewRequestArray.append(modelReviewRequestData)
    }
    
    func getRequestCount() -> Int{
        return modelReviewRequestArray.count
    }
    
    func getUserName(index:Int) ->String
    {
        let rv:ModelReviewRequestData = modelReviewRequestArray[index];
        
        return rv.name
    }

    func getModelReviewRequestDataObject(index:Int) -> ModelReviewRequestData
    {
        let rv:ModelReviewRequestData = modelReviewRequestArray[index];
        
        return rv
    }
    
}

class ModelReviewRequestData
{
    var id:String!
    var l_fname:String!
    var l_lname:String!
    var module_id:String!
    var course_id:String!
    var name:String!
}


