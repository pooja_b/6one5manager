//
//  LearnersDetailsService.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation
public class LearnersDetailsService : NSObject
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: LearnersDetailsService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = LearnersDetailsService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (LearnerDetails)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let login = Login.sharedInstance
        
    
        let defaults = UserDefaults.standard
        if let name = defaults.string(forKey: "id") {
            print(name)
        }
        let apiURL: String = WebAPI.MANAGERLEARNERLIST + login.getManagerId() // "https://api.enyotalms.com/lms/test/admin/api/v1/learners/41"
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            if (error != nil) {
                print("Error: \(error)")
                let learnerDetails = LearnerDetails.sharedInstance
                completionHandler(learnerDetails)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                //print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        
                        print("Print converted dictionary",convertedJsonIntoDict)
                        
                        
                        // Get value by key
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let learnerDetails = LearnerDetails.sharedInstance
                            //learnerDetails.message = convertedJsonIntoDict["message"] as? String
                            
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            completionHandler(learnerDetails)
                            
                        }
                        else
                        {
                            if(statusVal)!
                            {
                                let count = convertedJsonIntoDict["count"] as? Int
                                
                                //count = count! - 1
                                let learnerDetails = LearnerDetails.sharedInstance
                                var individualPerformance:IndividualPerformanceData!
                                for index in 0...count!
                                {
                                    if let module1Data = convertedJsonIntoDict["i\(index)"] as? NSDictionary
                                    {
                                        let fName: String = (String(describing: module1Data["l_fname"]!))
                                        let lName: String = (String(describing: module1Data["l_lname"]!))
                                        let lId:String = (String(describing: module1Data["l_id"]!))
                                        
                                        individualPerformance = IndividualPerformanceData()
                                        
                                        let json = JSON(convertedJsonIntoDict)
                                        print("here is the quiz dic count \(json["quiz"].dictionary?.count)")
                                        let quizData = module1Data["quiz"] as! NSDictionary
                                        print(quizData)
                                        if quizData.count > 0
                                        {
                                            if quizData ["Welcome"] != nil{
                                            individualPerformance.addQuizDataPoints(dataPoint: "Welcome")
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Welcome"]! as! Double)
                                            }
                                            if quizData ["Understand"] != nil{

                                            individualPerformance.addQuizDataPoints(dataPoint: "Understand")//Understand
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Understand"]! as! Double)
                                            }
                                            
                                            if quizData ["Understand"] != nil{
                                            individualPerformance.addQuizDataPoints(dataPoint: "Advise")
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Advise"]! as! Double)
                                            }
                                            
                                            if quizData ["Close"] != nil{
                                            individualPerformance.addQuizDataPoints(dataPoint: "Close")
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Close"]! as! Double)
                                            }
                                            
                                            if quizData ["Thank"] != nil{

                                            individualPerformance.addQuizDataPoints(dataPoint: "Thank")
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Thank"]! as! Double)
                                            }
                                            if quizData ["Handling Customer Complaints"] != nil{
                                            individualPerformance.addQuizDataPoints(dataPoint: "Handling Customer Complaints")
                                            individualPerformance.addQuizDataValues(dataValue: quizData["Handling Customer Complaints"]! as! Double)
                                            }
                                        }
                                        else
                                        {
                                            //print("learnerDetail quiz details data not found")
                                        }
                                        
                                        let reviewData = module1Data["review"] as! NSDictionary
                                        if reviewData.count > 0
                                        {
                                            
                                            
                                            if reviewData ["Welcome"] != nil{
                                                individualPerformance.addReviewDataPoints(dataPoint: "Welcome")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Welcome"]! as! Double)
                                            }
                                            if reviewData ["Understand"] != nil{individualPerformance.addReviewDataPoints(dataPoint: "Understand")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Understand"]! as! Double)
                                            }
                                            if reviewData ["Advise"] != nil{
                                            individualPerformance.addReviewDataPoints(dataPoint: "Advise")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Advise"]! as! Double)
                                            
                                        }
                                            if reviewData ["Close"] != nil{

                                        individualPerformance.addReviewDataPoints(dataPoint: "Close")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Close"]! as! Double)
                                            }
                                            if reviewData ["Thank"] != nil{
                                                individualPerformance.addReviewDataPoints(dataPoint: "Thank")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Thank"]! as! Double)
                                            }
                                            if reviewData ["Handling Customer Complaints"] != nil{
                                            individualPerformance.addReviewDataPoints(dataPoint: "Handling Customer Complaints")
                                            individualPerformance.addReviewDataValues(dataValue: reviewData["Handling Customer Complaints"]! as! Double)
                                            }
                                        }
                                        else
                                        {
                                            
                                        }
                                        let moduleStatus = module1Data["module_status"] as! NSDictionary
                                        
                                        if moduleStatus.count > 0
                                        {
                                            
                                            
                                            individualPerformance.addModuleStatusPoints(dataPoint: "Not Started")
                                            individualPerformance.addModuleStatusPoints(dataValue: moduleStatus["Not Started"]! as! Double)
                                            
                                            individualPerformance.addModuleStatusPoints(dataPoint: "In Progress")
                                            individualPerformance.addModuleStatusPoints(dataValue: moduleStatus["In Progress"]! as! Double)
                                            
                                            individualPerformance.addModuleStatusPoints(dataPoint: "Completed")
                                            individualPerformance.addModuleStatusPoints(dataValue: moduleStatus["Completed"]! as! Double)
                                        }
                                        else
                                        {
                                            
                                        }
                                        
                                        learnerDetails.addLearnerProfile(learnerProfile: fName + " " + lName, lId: lId)
                                        print("Quiz count data \( individualPerformance.quizDataPoints.count)")
                                        learnerDetails.addPerformance(object: individualPerformance)
                                        
                                    }
                                }
                                
                                completionHandler(learnerDetails)
                            }// end of if
                        }
                        
                        
                        
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
    }
    
}
