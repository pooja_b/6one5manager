//
//  ModuleDescriptorDetailVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 23/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ModuleDescriptorDetailVC: UIViewController {
    var descHeaderViewColor = UIColor()
    @IBOutlet weak var descriptorHeaderView: UIView!
    @IBOutlet weak var descriptorHeaderLabel: UILabel!
    @IBOutlet weak var descriptorLabel: UILabel!
    @IBOutlet weak var descriptorDetailLabel: UILabel!
    @IBOutlet weak var descriptorFooterView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func closeButton(_ sender: AnyObject) {
        self.view.alpha = 0
    }

    @IBAction func nextButtonAction(_ sender: AnyObject) {
    }
    @IBAction func previousButtonAction(_ sender: AnyObject) {
    }
    
    
    
}
