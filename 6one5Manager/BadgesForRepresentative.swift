//
//  File.swift
//  6one5Manager
//
//  Created by enyotalearning on 02/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class BadgesForRepresentative
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: BadgesForRepresentative
    {
        //2
        struct Singleton
        {
            //3
            static let instance = BadgesForRepresentative()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (BadgesData)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String = WebAPI.GETBADGESLIST + login.getManagerId() //WebAPI.MANAGERGETNOTIFS +
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let badgesData = BadgesData.sharedInstance
                completionHandler(badgesData)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                //print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let badgesData = BadgesData.sharedInstance
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                
                                //var count = convertedJsonIntoDict["count"] as? Int
                                //count = count! - 1
                                
                                let json = JSON(convertedJsonIntoDict)
                                var repBadge:RepresentativeBadges
                                for item in json["data"].arrayValue
                                {
                                    repBadge = RepresentativeBadges()
                                    
                                    if item["id"].exists()
                                    {
                                        repBadge.id = item["id"].stringValue
                                    }
                                    if item["badge_id"].exists()
                                    {
                                        repBadge.badgeId = item["badge_id"].stringValue
                                    }
                                    if item["l_fname"].exists()
                                    {
                                        repBadge.fristName = item["l_fname"].stringValue
                                    }
                                    if item["l_lname"].exists()
                                    {
                                        repBadge.lastName = item["l_lname"].stringValue
                                    }
                                    badgesData.addRepBadgesDetails(repBadgesDetails: repBadge)
                                    
                                }
                                
                                
                            }//end if
                        }
                        
                        completionHandler(badgesData)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
        
    }
    
    
    
}



