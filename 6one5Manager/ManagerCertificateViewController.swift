//
//  ManagerCertificateViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 30/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ManagerCertificateViewController: UIViewController {
    
    @IBOutlet weak var certificateMainView: UIView!
    let certificates = Certificate.sharedInstance
    
    @IBOutlet weak var certImageView: UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        certificateMainView.layer.shadowColor = UIColor.gray.cgColor
        certificateMainView.layer.shadowOpacity = 1
        certificateMainView.layer.shadowOffset = CGSize.zero
        certificateMainView.layer.shadowRadius = 3
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        loadCertificateData()
        
        
    }
    
    @IBAction func shareBtnTapped(_ sender: AnyObject)
    {
        //TODO: Share button funcanality
        
        let cert = certificates.certificates[0]
        //cert.image = UIImage(named: "cert.png")!
        // set up activity view controller
        let imageToShare = [ cert.image ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
    @IBAction func downloadBtnTapped(_ sender: AnyObject)
    {
        //TODO: Download button funcanality
        var certImage = certificates.certificates[0].image
        certImage = self.certImageView.image
        UIImageWriteToSavedPhotosAlbum(certImage!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
        
    }
    func loadCertificateData()
    {
        if (self.certificates.certificates.count >= 0 )
        {
            certificates.freeCertificateData()
        }
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        print("Starting the certificate service")
        CertificateService.sharedInstance.loadData(completionHandler:
            {(userData:Certificate) -> () in
                DispatchQueue.main.async() {
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                }

                DispatchQueue.main.async() {
                    
                    let login = Login.sharedInstance
                    
                    if login.status != nil
                    {
                        let alertController = UIAlertController(title:"User status", message: login.message , preferredStyle: .alert)
                        let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                            (action:UIAlertAction!) in
                           // self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                            let initViewController: LoginVC = storyboard.instantiateViewController(withIdentifier: "LoginVC") as UIViewController as! LoginVC
                            self.present(initViewController, animated: true, completion: nil)
                            
                        }
                        alertController.addAction(OKAction)
                        self.present(alertController, animated: true, completion:nil)
                        
                    }
                    //let cert = self.certificates.certificates[0]
                    //  self.certImageView.image = self.certificates.certificates[0].path
                    print("Stop the certificate service")
                    progressHUD.hide()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    let imagePath : String = self.certificates.certificates[0].path
                    
                    let url = URL(string : imagePath)
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    self.certImageView.image = UIImage(data: data!)
                    //set cert.image to outlet
                    
                }
        })
        
    }
    
    func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "saveerror".localized(), message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "ok".localized(), style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "saved".localized(), message: "imagesaved".localized(), preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "ok".localized(), style: .default))
            present(ac, animated: true)
        }
    }
    
    
}
