//
//  LevelsVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 01/10/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Charts
import Localize_Swift

@IBDesignable class LevelsVC: UIView {

    @IBOutlet weak var levelTitle: UILabel!
    @IBOutlet weak var levelChart: PieChartView!
     let salesTeamPerformanceData =  SalesTeamPerformanceData.sharedInstance
    var view:UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func loadViewFromNib() -> UIView {
        let bundle = Bundle(for:type(of: self))
        let nib = UINib(nibName: "LevelVC" , bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        levelTitle.text = "levelTitle".localized()
        
        return view
    }
    
    func setup() {
        view = loadViewFromNib()
        //view.frame = bounds
        //view.frame = self.frame
        //view.autoresizingMask = [.FlexibleWidth, .FlexibleHeight] //UIViewAutoresizing.FlexibleWidth | UIViewAutoresizing. FlexibleHeightaddSubview(view)
        view.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height)//CGRect(0, 0, self.frame.size.width, self.frame.size.height);
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        addSubview(view)
        
        let chart = ChartConfig.sharedInstance
        print("Level chart ")
        
        for i in 0 ..< salesTeamPerformanceData.levelPerformanceDataValues.count {
            print("Level data of values  \(i) is : \(salesTeamPerformanceData.levelPerformanceDataValues[i])")
        }
        
        for i in 0 ..< salesTeamPerformanceData.levelPerformanceDataPoints.count {
            print("Level data of DataPoints \(i) is : \(salesTeamPerformanceData.levelPerformanceDataPoints[i])")
        }
        
        chart.setPieChartData(dataPoints: salesTeamPerformanceData.levelPerformanceDataPoints, values: salesTeamPerformanceData.levelPerformanceDataValues, chartObject: levelChart, chartLabel: "Chart 4")
         levelChart.chartDescription?.text = ""
         levelChart.legend.horizontalAlignment = .center
         //chart4.legend.font = UIFont.systemFontSize(CGFloat(12))
        levelChart.dragDecelerationEnabled = false
        levelChart.rotationEnabled = false
        
        
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if scrollView.contentOffset.x>0 {
            scrollView.contentOffset.x = 0
            scrollView.contentOffset.y = 0
        }
    }
}
