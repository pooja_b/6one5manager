//
//  ReviewInfoViewController.swift
//  6one5Manager
//
//  Created by enyotalearning on 21/12/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit

class ReviewInfoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableview: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func closeButton(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell :UITableViewCell!
        if indexPath.row == 0 {
            
            cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_1", for: indexPath)
        }
        else if indexPath.row == 1 {
            
            cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_2", for: indexPath)
            
        }else if indexPath.row == 2 {
            
            cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_3", for: indexPath)
            
        }else if indexPath.row == 3 {
            
            cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_4", for: indexPath)
        }
        else {
            
            cell = tableview.dequeueReusableCell(withIdentifier: "ReviewInfoTableViewCell_5", for: indexPath)
        }
        return cell
    }
}
