//
//  NotificationService.swift
//  6one5Manager
//
//  Created by enyotalearning on 24/11/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import Foundation

class NotificationService
{
    
    //using Singleton pattern for single object handling...
    class var sharedInstance: NotificationService
    {
        //2
        struct Singleton
        {
            //3
            static let instance = NotificationService()
        }
        //4
        return Singleton.instance
    }
    
    func loadData(completionHandler:@escaping (NotificationData)->())
    {
        let headers = [
            "cache-control": "no-cache",
            ]
        let login = Login.sharedInstance
        let apiURL: String = WebAPI.USERNOTIFICATION + login.getManagerId()
        
        let request = NSMutableURLRequest(url: NSURL(string: apiURL)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("Error: \(error)")
                let notificationData = NotificationData.sharedInstance
                completionHandler(notificationData)
            } else
            {
                //let httpResponse = response as? HTTPURLResponse
                //print("HTTP Response for the learner list @@@@ : \(data)")
                
                do
                {
                    if let convertedJsonIntoDict = try JSONSerialization.jsonObject(with: data!, options: []) as? NSDictionary
                    {
                        let notificationData = NotificationData.sharedInstance
                        
                        let statusVal = convertedJsonIntoDict["status"] as? Bool
                        if statusVal == nil
                        {
                            print("status val is null")
                            let statusVal = convertedJsonIntoDict["status"] as? String
                            print("status valule is \(statusVal)")
                            let login = Login.sharedInstance
                            login.status = String(describing: convertedJsonIntoDict["status"] as? Bool)
                            login.message = convertedJsonIntoDict["message"] as? String
                            
                            
                        }
                        else
                        {
                            
                            if(statusVal)!
                            {
                                
                                //var count = convertedJsonIntoDict["count"] as? Int
                                //count = count! - 1
                                
                                let json = JSON(convertedJsonIntoDict)
                                var notificationDetails:NotificationDetails
                                for item in json["data"].arrayValue
                                {
                                    notificationDetails = NotificationDetails()
                                    
                                    notificationDetails.id = item["id"].stringValue
                                    notificationDetails.name = item["name"].stringValue
                                    notificationDetails.content = item["content"].stringValue
                                    notificationDetails.notification_id = item["notification_id"].stringValue
                                    notificationDetails.user_id = item["user_id"].stringValue
                                    notificationDetails.badge_id = item["badge_id"].stringValue
                                    notificationDetails.course_id = item["course_id"].stringValue
                                    notificationDetails.module_id = item["module_id"].stringValue
                                    notificationDetails.representative_id = item["representative_id"].stringValue
                                    notificationDetails.notification_type = item["notification_type"].stringValue
                                    
                                    notificationData.addNotificationDetails(notificationDetails: notificationDetails)
                                    
                                }
                                
                                
                            }//end of if
                            
                        }
                        completionHandler(notificationData)
                    }
                    
                } catch let error as NSError
                {
                    print(error)
                }
                
                
            }
            
            
        })
        
        dataTask.resume()
        
    }
    
    
    
}
