//
//  PerformanceVC.swift
//  6one5Manager
//
//  Created by enyotalearning on 29/09/16.
//  Copyright © 2016 enyotalearning. All rights reserved.
//

import UIKit
import Dispatch.base                  // 和 swift2.2 比还是一行代码，但是内容有所不同
import Dispatch.block                 // 和 swift2.2 比 只剩下一个结构体，加了 __ , 私有化了
import Dispatch.data                  // 和 swift 2.2 ，全部加了 __ , 私有化了
import Dispatch.group                 // 添加了 DispatchGroup 对象， 对之前的函数全部加了 __ , 私有化了
import Dispatch.io                    //  添加了 DispatchIO， 之前的函数全部进行了__, 私有化了。
import Dispatch.object                // 添加了DispatchObject 对象，函数全部不见了
import Dispatch.once                  // 基本不可用
import Dispatch.queue                 // 添加了 DispatchQueue， 之前的函数全部加了__ , 私有化了。
import Dispatch.semaphore             // 添加了 DispatchSemaphore， 之前函数全部加了__, 私有化了。
import Dispatch.source                // 添加了DispatchSource 对象， 之前函数全部加了__, 私有化了
import Dispatch.time                  // 添加了DispatchSource 对象， 之前函数全部加了__, 私有化了
import Dispatch
import os_object

import Localize_Swift

class PerformanceVC: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    //UISearchBarDelegate, UISearchBarDelegate, UITextFieldDelegate
    let learnerDetails =  LearnerDetails.sharedInstance
    //let individualPerformanceData =  IndividualPerformanceData.sharedInstance
    //var filteredArray = [String]()
    
    var shouldShowSearchResults = false
    
   // @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    //var searchActive : Bool = false
    //var filtered:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Setup delegates */
        tableView.delegate = self
        tableView.dataSource = self
        //searchBar.delegate = self
        
        //self.hideKeyboardWhenTappedAround()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        //self.title = "performance".localized()
       
        
        let progressHUD = ProgressHUD()
        progressHUD.setCustomText(text: "loadingdata".localized())
        self.view.addSubview(progressHUD)
        progressHUD.activityIndictor.color = UIColor.black
        
        UIApplication.shared.beginIgnoringInteractionEvents()
        /* if (self.individualPerformanceData.getQuizDataCount() == 0 )
         {
         
         clearIndividualData()
         
         }// end of checking
         
         */
        if (self.learnerDetails.getLearnerCount() > 0 )
        {
            //print("lerner details Data Reset ")
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerProfileArray = [String]()
            learnerDetails.learnerId = [String]()
            learnerDetails.selectedLearner = -1
            learnerDetails.performanceData = [IndividualPerformanceData]()
        }
        
        LearnersDetailsService.sharedInstance.loadData(completionHandler: { (userData:LearnerDetails) -> () in
            DispatchQueue.main.async()
                {
            UIApplication.shared.endIgnoringInteractionEvents()
            progressHUD.hide()
            }
            if (self.learnerDetails.getLearnerCount() > 0 )
            {
                let login = Login.sharedInstance
                if login.status != nil
                {
                    let alertController = UIAlertController(title: "User status", message: login.message , preferredStyle: .alert)
                    let OKAction = UIAlertAction(title: "ok".localized(), style: .default) {
                        
                        (action:UIAlertAction!) in
                        login.logoutFromDeviceCache()
                        self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                    }
                    alertController.addAction(OKAction)
                    self.present(alertController, animated: true, completion:nil)
                }else{
                    DispatchQueue.main.async()
                        {
                            //self.imageView.image = image;
                            progressHUD.hide()
                            UIApplication.shared.endIgnoringInteractionEvents()
                            self.tableView.reloadData()
                    } //learner details service completed
                }
            }// end if learnerDetails.getLearnerCount
            else
            {
                progressHUD.hide()
                UIApplication.shared.endIgnoringInteractionEvents()
                //self.tableView.reloadData()
            } // end else learnerDetails.getLearnerCount
            //}
            
        })
        
        
        
        
    }
    
    /*func loadDataLernerDetails()
    {
        if (self.learnerDetails.getLearnerCount() == 0 )
        {
            learnerDetails.learnerProfileArray = [String]()
        }
        
        LearnersDetailsService.sharedInstance.loadData(completionHandler: { (userData:LearnerDetails) -> () in
            
            if (self.learnerDetails.getLearnerCount() > 0 )
            {
                DispatchQueue.main.async()
                    {
                        //self.imageView.image = image;
                        self.tableView.reloadData()
                } //learner details service completed
                
            }// end if learnerDetails.getLearnerCount
            else
            {
                self.tableView.reloadData()
                
            } // end else learnerDetails.getLearnerCount
            
        })
    }*/
    
    
    /*func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchActive = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchActive = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filtered = learnerDetails.learnerProfileArray.filter({ (text) -> Bool in
            let tmp: NSString = text as NSString
            let range = tmp.range(of: searchText, options: NSString.CompareOptions.caseInsensitive)
            return range.location != NSNotFound
        })
        if(filtered.count == 0){
            searchActive = false;
        } else {
            searchActive = true;
        }
        self.tableView.reloadData()
    }
    */
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        /*if(searchActive) {
            return filtered.count
        }*/
        return learnerDetails.learnerProfileArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")! as UITableViewCell;
        /*if(searchActive){
            cell.textLabel?.text = filtered[(indexPath as NSIndexPath).row]
        } else {
            cell.textLabel?.text = learnerDetails.learnerProfileArray[(indexPath as NSIndexPath).row];
        }*/
        
        cell.textLabel?.text = learnerDetails.learnerProfileArray[indexPath.row];
        
        return cell;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        
        let indexPath = tableView.indexPathForSelectedRow //optional, to get from any UIButton for example
        
        //let currentCell = tableView.cellForRow(at: indexPath!)! as UITableViewCell
        
        let selectedIndex:Int = (indexPath?.row)!
        learnerDetails.setSelectedLearner(index: selectedIndex)
        let lernerid:String = learnerDetails.getLearnerId(index: selectedIndex)
        //let reviewNotificationData = ReviewNotificationData.sharedInstance
        //reviewNotificationData.setCureentUserId(id: lernerid)
        learnerDetails.setSelectedLearnerId(id: lernerid)
        learnerDetails.navigationPath = "1"
        //print("####Lerner id is : \(lernerid)")
        //print("Prepare for Segue : \(selectedIndex)")
    }
    
    
    
}
